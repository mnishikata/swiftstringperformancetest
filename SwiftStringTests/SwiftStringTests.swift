//
//  SwiftStringTests.swift
//  SwiftStringTests
//
//  Created by Masatoshi Nishikata on 30/08/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import XCTest
@testable import SwiftString

private let KENPO_NAME = "日本国憲法"
private let SOZEI_NAME = "租税特別措置法"
private let RESOURCE_URL = Bundle.main.url(forResource: SOZEI_NAME, withExtension: "html")!

class SwiftStringTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  private func readingString(at url:URL) -> String {
    let string = try? String(contentsOf: url, encoding: .shiftJIS)
    XCTAssert(string != nil )
    return string!
  }
  
  private func readingNSString(at url:URL) -> NSString {
    let string = try? NSString(contentsOf: url, encoding: String.Encoding.shiftJIS.rawValue)
    XCTAssert(string != nil )
    return string!
  }
  
  //MARK:- READING
  func testReadingString() {
    self.measure {
      _ = self.readingString(at: RESOURCE_URL)
    }
  }
  
  func testReadingNSString() {
    self.measure {
      _ = self.readingNSString(at: RESOURCE_URL)
    }
    
    XCTAssert(readingString(at: RESOURCE_URL) == readingNSString(at: RESOURCE_URL) as String )
  }
  
  //MARK:- COUNTING
  func testCountingString() {
    let string = self.readingString(at: RESOURCE_URL)
    self.measure {
      let count = string.characters.count
      print("counting String \(count)")
      // UTF16View DOES NOT comforms to RandomAccessCollection... count costs O(n)
    }
  }
  
  func testCountingStringUTF16() {
    let string = self.readingString(at: RESOURCE_URL)
    self.measure {
      let count = string.utf16.count
      print("counting utf16 \(count)")
      // UTF16View comforms to RandomAccessCollection... count costs O(1)
    }
  }
  
  func testCountingNSString() {
    let string = self.readingNSString(at: RESOURCE_URL)
    
    self.measure {
      let count = string.length
      print("counting NSString \(count)")
    }
  }

  //MARK:- FINDING
  func testFindingString() {
    let string = self.readingString(at: RESOURCE_URL)

    self.measure {
      var count = 0
      var range = string.startIndex ..< string.endIndex
      while let foundRange = string.range(of: "あ", options: [], range: range, locale: nil) {
        count += 1
        range = foundRange.upperBound ..< string.endIndex
      }
      print("testFindingString count \(count)")
    }
  }
  
  func testFindingNSString() {
    let string = self.readingNSString(at: RESOURCE_URL)

    self.measure {
      var count = 0

      var range = NSMakeRange(0, string.length)
      while true {
        let foundRange = string.range(of: "あ", options: [], range: range, locale: nil)
        if foundRange.location == NSNotFound { break }
        count += 1
        range.location = NSMaxRange(foundRange)
        range.length = string.length - NSMaxRange(foundRange)
      }
      print("testFindingNSString count \(count)")

    }
  }
  
  //MARK:- REGEX SEARCH
  func testRegexString() {
    let string = self.readingString(at: RESOURCE_URL)

    self.measure {
      var count = 0
      var range = string.startIndex ..< string.endIndex
      while let foundRange = string.range(of: "[あ-ん]+", options: [.regularExpression], range: range, locale: nil) {
        count += 1
        range = foundRange.upperBound ..< string.endIndex
      }
      print("testRegexString count \(count)")
    }
  }
  
  func testRegexNSString() {
    let string = self.readingNSString(at: RESOURCE_URL)

    self.measure {
      var count = 0
      
      var range = NSMakeRange(0, string.length)
      while true {
        let foundRange = string.range(of: "[あ-ん]+", options: [.regularExpression], range: range, locale: nil)
        if foundRange.location == NSNotFound { break }
        count += 1
        range.location = NSMaxRange(foundRange)
        range.length = string.length - NSMaxRange(foundRange)
      }
      print("testRegexNSString count \(count)")
      
    }
  }
  
  //MARK:- REPLACING
  private func repaceString(_ string:String) -> String {
    var count = 0
    var string = string
    var range = string.startIndex ..< string.endIndex
    var ranges:[Range<String.Index>] = []
    while let foundRange = string.range(of: "あ", options: [], range: range, locale: nil) {
      count += 1
      ranges.append(foundRange)
      range = foundRange.upperBound ..< string.endIndex
    }
    ranges.reversed().forEach {
      string.replaceSubrange($0, with: "のの")
    }
    print("testReplacingString count \(count)")
    return string
  }
  
  private func repaceStringUTF16(_ string:String) -> String {
    var count = 0
    var string = string
    var range = string.startIndex ..< string.endIndex
    var ranges:[Range<Int>] = []
    while let foundRange = string.range(of: "あ", options: [], range: range, locale: nil) {
      count += 1
      let from = String.UTF16View.Index(foundRange.lowerBound, within: string.utf16)
      let to = String.UTF16View.Index(foundRange.upperBound, within: string.utf16)
      ranges.append( from.distance(to: string.utf16.startIndex) ..< to.distance(to: string.utf16.startIndex)  )
      range = foundRange.upperBound ..< string.endIndex
    }
    
    var utf16Array = Array(string.utf16)
    ranges.reversed().forEach {
      utf16Array.replaceSubrange($0, with: Array("のの".utf16))
    }
    string = String(utf16CodeUnits: utf16Array, count: utf16Array.count )
    return string
  }
  
  private func repaceNSString(_ string:NSMutableString) {
    var count = 0
    var range = NSMakeRange(0, string.length)
    let ranges = NSMutableArray()
    while true {
      let foundRange = string.range(of: "あ", options: [], range: range, locale: nil)
      if foundRange.location == NSNotFound { break }
      count += 1
      ranges.insert(NSValue(range:foundRange), at: 0)
      range.location = NSMaxRange(foundRange)
      range.length = string.length - NSMaxRange(foundRange)
    }
    for rangeObj in ranges {
      let range = (rangeObj as! NSValue).rangeValue
      string.replaceCharacters(in: range, with: "のの")
    }
    
    print("testReplacingNSString count \(count)")
  }
  
  func testReplacingString() {
    let string = readingString(at: RESOURCE_URL)
    self.measure {
      _ = self.repaceString(string)
    }
  }
  
  func testReplacingStringUTF16() {
    let string = readingString(at: RESOURCE_URL)
    var replacedString:String? = nil

    self.measure {
      replacedString = self.repaceStringUTF16(string)
    }
    
    let swiftString = readingString(at: RESOURCE_URL)
    XCTAssert(replacedString! as String == self.repaceString(swiftString) )

  }
  
  func testReplacingNSString() {
    let string = readingNSString(at: RESOURCE_URL)
    var replacedString:NSMutableString? = nil
    self.measure {
      replacedString = string.mutableCopy() as? NSMutableString
      self.repaceNSString(replacedString!)
    }
    
    let swiftString = readingString(at: RESOURCE_URL)
    XCTAssert(replacedString! as String == self.repaceString(swiftString) )
  }
  
  //MARK:- MAKING SUBSTRINGS
  func testExtractRandomLocationInString() {
    let string = readingString(at: RESOURCE_URL)
    let count = UInt32(string.characters.count)
    var letters:[String] = []
    self.measure {
      for _ in 0 ..< 100 {
        let random = arc4random_uniform(count)
        let index = string.index(string.startIndex, offsetBy: Int(random))
        let letter = string.substring(with: index ..< string.index(after: index))
        letters.append(letter)
      }
    }
  }
  
  func testExtractRandomLocationInStringUTF16() {
    let string = readingString(at: RESOURCE_URL)
    let count = UInt32(string.characters.count)
    var letters:[String] = []
    self.measure {
      for _ in 0 ..< 100 {
        let random = arc4random_uniform(count)
        let utf16Index = String.UTF16View.Index(Int(random))
        let array = Array([string.utf16[utf16Index]])
        let letter = String(utf16CodeUnits: array, count: 1 )
        letters.append(letter)
      }
    }
  }
  
  func testExtractRandomLocationInNSString() {
    let string = readingNSString(at: RESOURCE_URL)
    let count = UInt32(string.length)
    let letters = NSMutableArray()
    self.measure {
      for _ in 0 ..< 100 {
        let random = arc4random_uniform(count)
        let letter = string.substring(with: NSMakeRange(Int(random), 1))
        letters.add(letter)
      }
    }
  }

  
}
